class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :title
      t.datetime :published
      t.string :url
      t.integer :feed_id
      t.text :description

      t.timestamps null: false
    end
  end
end
