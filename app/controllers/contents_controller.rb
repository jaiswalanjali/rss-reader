require 'feed'
class ContentsController < ApplicationController
  before_action :set_feed, only: :index
  def index
    @feeds.each do |feed|
      FeedData.update_feeds_contents(feed)
    end
    @contents = Content.all.order('published desc')
  end

  def show
    @content = Content.find(params[:id])
  end

  private

  def set_feed
    @feeds = Feed.all
  end
end
