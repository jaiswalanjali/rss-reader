require 'feed'
class Feed < ActiveRecord::Base
  has_many :contents, dependent: :destroy
  after_create do
    FeedData.update_feed(url)
  end
end
