require 'open-uri'
require 'rss'
module FeedData
  def self.update_feed(url)
    parsed_feed = RSS::Parser.parse(open(url).read, false).channel
    Feed.find_by_url(url).update_attributes(
      title: parsed_feed.title,
      description: parsed_feed.description
    )
  end

  def self.update_feeds_contents(feed)
    parsed_feed = RSS::Parser.parse(open(feed.url).read, false).items[0..5]
    parsed_feed.each do |result|
      local_content = feed.contents.where(title: result.title).first_or_initialize
      local_content.update_attributes(
        description: result.description,
        url: result.link,
        published: result.pubDate
      )
    end
  end
end
